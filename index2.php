<!doctype html><!doctype html>
<html lang="en">
  <head>


    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.88.1">
    <title>香港生態旅遊專業培訓中心</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/headers/">

    

    <!-- Bootstrap core CSS -->
    <link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">

   
    <!-- Custom styles for this template -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link href="css/headers.css" rel="stylesheet" type="text/css">
    <link href="css/main.css" rel="stylesheet" type="text/css">
  </head>
  <body>
  

  
  <header class="p-3 bg-success text-white">
    <div class="container">
      <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">


        <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
     
          <li><h3>香港生態旅遊專業培訓中心</h3><h6>Hong Kong Ecotourism & Travels Professional Training Centre</h6></li>

        </ul>

        <form class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3">
          <input type="search" class="form-control form-control-white searchstyle" placeholder="請輸入課程或講座名稱" aria-label="Search">
        </form>

        <div class="text-end">
          <p class="nav2" style="color:white;">
            <a href="#">學員專區</a> | <a href="#">導師專區</a>
            <span style="margin-left: 20px;font-size:10px">關注我們</span> 
          <a href="https://www.facebook.com/ETTCHK"> <img src="image/Facebook.png" width="30px "></a>
          <a href="#"><img href="#" src="image/instagram Color.png" width="30px "></a>
          <a href="https://www.youtube.com/channel/UCAXg8BSXPgqh6pb5M2pBoxA"><img src="image/YouTube Color.png" width="30px"></a>
        
        </div>
      </div>
      
    </div>


    <nav class="navbar navbar-expand-lg navbar-light"  style="margin-top:-10px;">
      <a class="navbar-brand" href=""></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    <div class="collapse navbar-collapse my-4" id="navbarNav">
      <ul class="navbar-nav mx-auto">
      <li class="nav-item" style="border: 1px solid rgb(177, 172, 172);">
        <a class="nav-link active" aria-current="page" href="index.html"><img style="margin-top:-5px;width: 17px;" src="image/Menu Bar Icon/Home.png"> 主 頁</a>
      </li>
        <li class="nav-item" style="border: 1px solid rgb(177, 172, 172);">
          <a class="nav-link active" aria-current="page" href="#"><img style="margin-top:-2px;width: 17px;" src="image/Menu Bar Icon/About Us.png"> 關於我們</a>
        </li>
        <li class="nav-item " style="border: 1px solid rgb(177, 172, 172);">
          <a class="nav-link active" aria-current="page" href="index2.html"><img style="margin-top:-2px;width: 17px;" src="image/Menu Bar Icon/Courses Overview.png"> 課程總覽</a>
        </li>
        <li class="nav-item" style="border: 1px solid rgb(177, 172, 172);">
          <a class="nav-link active" aria-current="page" href="#"><img style="margin-top:-4px;width: 17px;" src="image/Menu Bar Icon/Speech.png"> 講座活動</a>
        </li>
        <li class="nav-item" style="border: 1px solid rgb(177, 172, 172);">
          <a class="nav-link active" aria-current="page" href="#"><img style="margin-top:-3px;width: 17px;" src="image/Menu Bar Icon/Group Class.png"> 包班/項目</a>
        </li>  
        <li class="nav-item" style="border: 1px solid rgb(177, 172, 172);">
          <a class="nav-link active" aria-current="page" href="#"><img style="margin-top:-3px;width: 17px;" src="image/Menu Bar Icon/Centre.png"> 中心動態</a>
        </li>  
        <li class="nav-item" style="border: 1px solid rgb(177, 172, 172);">
          <a class="nav-link active" aria-current="page" href="#"><img style="margin-top:-3px;width: 17px;" src="image/Menu Bar Icon/FAQ.png"> 常見問題</a>
        </li>  
        <li class="nav-item" style="border: 1px solid rgb(177, 172, 172);">
          <a class="nav-link active" aria-current="page" href="#"><img style="margin-top:-3px;width: 17px;" src="image/Menu Bar Icon/Contact Us.png"> 聯絡我們</a>
        </li>  
      </ul>
    </div>
  </nav>
</header>

  
<article>

          
        
<div class="container px-4 py-5" id="custom-cards">
  <div class=py-5>
    <div class="card mb-3" style="max-width: 1220px;">
    <div class="row g-0">    
      <div class="col-md-8">
        <div class="card-body">
        </div>
      </div>
     </div>
              </div>
            </div>
          </div>
        </div>

<div class="container" id="colaccordion">
  <div class="row">
    <div class="col-md-8" >
        <nav>
          <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">課程內容概覽</button>
            <button class="nav-link" id="nav-timetable-tab" data-bs-toggle="tab" data-bs-target="#nav-timetable" type="button" role="tab" aria-controls="nav-timetable" aria-selected="false">課程時間表</button>
            <button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab" data-bs-target="#nav-contact" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">導師及顧問</button>
            <button class="nav-link" id="nav-submit-tab" data-bs-toggle="tab" data-bs-target="#nav-submit" type="button" role="tab" aria-controls="nav-submit" aria-selected="false">報名需知</button>
            <button class="nav-link" id="nav-cef-tab" data-bs-toggle="tab" data-bs-target="#nav-cef" type="button" role="tab" aria-controls="nav-cef" aria-selected="false">CEF 注意事項</button>
            <button class="nav-link" id="nav-student-tab" data-bs-toggle="tab" data-bs-target="#nav-student" type="button" role="tab" aria-controls="nav-student" aria-selected="false">學員心聲</button>
      
          </div>
        </nav>
</div>
<!-- ------ -->


      <div class="col-md-4 text-end">

        3333
      </div>

      </div>

</div>

 </article>




  <footer class="py-5 bg-success">
  <div class="container">
  
      <div class="row">
        <div class="col-4">
          
          <ul class="nav flex-column">
            <li class="nav-item mb-2 footername1">香港生態旅遊專業培訓中心<li>
              <li class="nav-item mb-2 footername2">Hong Kong Ecotourism & Travels Professional Training Centre</li>
              <li class="nav-item mb-2 address">九龍荔枝角青山道682-684號 潮流工貿中心30樓9-10室</li>
              <li class="nav-item mb-2 address">Tel:(852) 2116 1971    Fax:(852) 2465 9986</li>
              <li class="nav-item mb-2 address">Email:info@ettc.hk</li>
          </ul>
        </div>

        <div class="col-1 address">
          
          <ul class="nav flex-column ">
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">關於我們</a></li>
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">課程總覽</a></li>
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">講座活動</a></li>
           
          </ul>
        </div>
  
        <div class="col-1 address">
          
          <ul class="nav flex-column">
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">包班/頂目</a></li>
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">活動花絮</a></li>
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">常目問題</a></li>
          
          </ul>
        </div>
  
        <div class="col-1 address">
          
          <ul class="nav flex-column">
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">學員專區</a></li>
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">導師專區</a></li>
          
           
          </ul>
        </div>
  
    
      </div>
  

      <div class="border-top justify-content-between">
      <!-- <div class="d-flex justify-content-between py-4 my-4 border-top"> -->
        <p>&copy; 2021 Company, Inc. All rights reserved.</p>
        <!-- <ul class="list-unstyled d-flex">
          <li class="ms-3"><a class="link-dark" href="#"><svg class="bi" width="24" height="24"><use xlink:href="#twitter"/></svg></a></li>
          <li class="ms-3"><a class="link-dark" href="#"><svg class="bi" width="24" height="24"><use xlink:href="#instagram"/></svg></a></li>
          <li class="ms-3"><a class="link-dark" href="#"><svg class="bi" width="24" height="24"><use xlink:href="#facebook"/></svg></a></li>
        </ul> -->
      </div>
    </footer>
  </div>
  
  



  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>

      
  </body>
</html>