<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.88.1">
    <title>香港生態旅遊專業培訓中心</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/headers/">

    

    <!-- Bootstrap core CSS -->
<link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">


    
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="css/headers.css" rel="stylesheet" type="text/css">
    <link href="css/main.css" rel="stylesheet" type="text/css">
  </head>
  <body>
  

  
  <header class="p-3 bg-success ">
    <div class="container">
      <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
  
        <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
         
          <li><h3>香港生態旅遊專業培訓中心</h3><h6>Hong Kong Ecotourism & Travels Professional Training Centre</h6></li>

        </ul>

        <form class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3">
          <input type="search" class="form-control form-control-white searchstyle" placeholder="請輸入課程或講座名稱" aria-label="Search">
        </form>

        <div class="text-end">
          <p class="nav2" style="color:white;">
            <a href="#">學員專區</a> | <a href="#">導師專區</a>
            <span style="margin-left: 20px;font-size:10px">關注我們</span> 
          <a href="https://www.facebook.com/ETTCHK"> <img src="image/Facebook.png" width="30px "></a>
          <a href="#"><img href="#" src="image/instagram Color.png" width="30px "></a>
          <a href="https://www.youtube.com/channel/UCAXg8BSXPgqh6pb5M2pBoxA"><img src="image/YouTube Color.png" width="30px"></a>
        
        </div>
      </div>
      
    </div>


    <nav class="navbar navbar-expand-lg navbar-light"  style="margin-top:-10px;">
      <a class="navbar-brand" href=""></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    <div class="collapse navbar-collapse my-4" id="navbarNav">
      <ul class="navbar-nav mx-auto">
      <li class="nav-item" style="border: 1px solid rgb(177, 172, 172);">
        <a class="nav-link active" aria-current="page" href="index.html"><img style="margin-top:-5px;width: 17px;" src="image/Menu Bar Icon/Home.png"> 主 頁</a>
      </li>
        <li class="nav-item" style="border: 1px solid rgb(177, 172, 172);">
          <a class="nav-link active" aria-current="page" href="#"><img style="margin-top:-2px;width: 17px;" src="image/Menu Bar Icon/About Us.png"> 關於我們</a>
        </li>
        <li class="nav-item " style="border: 1px solid rgb(177, 172, 172);">
          <a class="nav-link active" aria-current="page" href="index2.html"><img style="margin-top:-2px;width: 17px;" src="image/Menu Bar Icon/Courses Overview.png"> 課程總覽</a>
        </li>
        <li class="nav-item" style="border: 1px solid rgb(177, 172, 172);">
          <a class="nav-link active" aria-current="page" href="#"><img style="margin-top:-4px;width: 17px;" src="image/Menu Bar Icon/Speech.png"> 講座活動</a>
        </li>
        <li class="nav-item" style="border: 1px solid rgb(177, 172, 172);">
          <a class="nav-link active" aria-current="page" href="#"><img style="margin-top:-3px;width: 17px;" src="image/Menu Bar Icon/Group Class.png"> 包班/項目</a>
        </li>  
        <li class="nav-item" style="border: 1px solid rgb(177, 172, 172);">
          <a class="nav-link active" aria-current="page" href="#"><img style="margin-top:-3px;width: 17px;" src="image/Menu Bar Icon/Centre.png"> 中心動態</a>
        </li>  
        <li class="nav-item" style="border: 1px solid rgb(177, 172, 172);">
          <a class="nav-link active" aria-current="page" href="#"><img style="margin-top:-3px;width: 17px;" src="image/Menu Bar Icon/FAQ.png"> 常見問題</a>
        </li>  
        <li class="nav-item" style="border: 1px solid rgb(177, 172, 172);">
          <a class="nav-link active" aria-current="page" href="#"><img style="margin-top:-3px;width: 17px;" src="image/Menu Bar Icon/Contact Us.png"> 聯絡我們</a>
        </li>  
      </ul>
    </div>
</nav>
  </header>

  
<article>

  <!-- <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-indicators">
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
    </div> -->
    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
        <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img src="image/section1pic/LMS Banner.png" class="d-block w-100" style="height: 550px;overflow: hidden;" alt="...">
          <div class="carousel-caption d-none d-md-block">
            <h5>First slide label</h5>
            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
          </div>
        </div>
        <div class="carousel-item">
          <img src="image/section1pic/bird.jpeg" class="d-block w-100" style="height: 550px;overflow: hidden;" alt="...">
          <div class="carousel-caption d-none d-md-block">
            <h5>Second slide label</h5>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          </div>
        </div>
        <div class="carousel-item">
          <img src="image/section1pic/dog.jpeg" class="d-block w-100" style="height: 550px;overflow: hidden;" alt="...">
          <div class="carousel-caption d-none d-md-block">
            <h5>Third slide label</h5>
            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    
    <!-- <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="image/section1pic/LMS Banner.png" class="d-block mw-100" style="height: 550px;overflow: hidden;" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5>First slide label</h5>
          <p>Some representative placeholder content for the first slide.</p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="image/section1pic/bird.jpeg" class="d-block w-100" style="height: 550px;overflow: hidden;" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5>Second slide label</h5>
          <p>Some representative placeholder content for the second slide.</p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="image/section1pic/dog.jpeg" class="d-block w-100" style="height: 550px;overflow: hidden;" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5>Third slide label</h5>
          <p>Some representative placeholder content for the third slide.</p>
        </div>
      </div>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </button>
  </div> -->


  <div class="container px-4 py-5" id="custom-cards">
    <!-- <h2 class="pb-2 border-bottom">Custom cards</h2> -->
     <!-- <img src="image/section1pic/bird.jpeg" style="width: 1350px;height: 470px;object-fit: cover;margin:auto;padding:30px;"> -->
      <h1 class="text-center mt-4 text-white">
       <span class="rounded p-2 fontdesign1">最</span>
       <span class="rounded p-2 fontdesign1">新</span>
       <span class="rounded p-2 fontdesign1">消</span>
       <span class="rounded p-2 fontdesign1">息</span>
      </h1>
      <div class=py-5>
      <div class="card mb-3" style="max-width: 1220px;">
        <div class="row g-0">
          <div class="col-md-4">
            <img src="image/170483779_4023562454368045_2012127977267701098_n.jpg" class="img-fluid rounded-start" alt="...">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
              <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>

  <div class="container px-4 py-5" id="custom-cards">
      <h1 class="text-center mt-4 text-white">
       <span class="rounded p-2 fontdesign2">熱</span>
       <span class="rounded p-2 fontdesign2">門</span>
       <span class="rounded p-2 fontdesign2">課</span>
       <span class="rounded p-2 fontdesign2">程</span>
      </h1>




    <div class="row row-cols-1 row-cols-lg-3 align-items-stretch g-4 py-5">
      <div class="col">
        <div class="card card-cover h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg" style="background-image: url('unsplash-photo-1.jpg');">
          <div class="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1">
            <h2 class="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold">Short title, long jacket</h2>
            <ul class="d-flex list-unstyled mt-auto">
              <li class="me-auto">
                <img src="https://github.com/twbs.png" alt="Bootstrap" width="32" height="32" class="rounded-circle border border-white">
              </li>
              <li class="d-flex align-items-center me-3">
                <svg class="bi me-2" width="1em" height="1em"><use xlink:href="#geo-fill"/></svg>
                <small>Earth</small>
              </li>
              <li class="d-flex align-items-center">
                <svg class="bi me-2" width="1em" height="1em"><use xlink:href="#calendar3"/></svg>
                <small>3d</small>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div class="col">
        <div class="card card-cover h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg" style="background-image: url('unsplash-photo-2.jpg');">
          <div class="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1">
            <h2 class="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold">Much longer title that wraps to multiple lines</h2>
            <ul class="d-flex list-unstyled mt-auto">
              <li class="me-auto">
                <img src="https://github.com/twbs.png" alt="Bootstrap" width="32" height="32" class="rounded-circle border border-white">
              </li>
              <li class="d-flex align-items-center me-3">
                <svg class="bi me-2" width="1em" height="1em"><use xlink:href="#geo-fill"/></svg>
                <small>Pakistan</small>
              </li>
              <li class="d-flex align-items-center">
                <svg class="bi me-2" width="1em" height="1em"><use xlink:href="#calendar3"/></svg>
                <small>4d</small>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div class="col">
        <div class="card card-cover h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg" style="background-image: url('unsplash-photo-3.jpg');">
          <div class="d-flex flex-column h-100 p-5 pb-3 text-shadow-1">
            <h2 class="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold">Another longer title belongs here</h2>
            <ul class="d-flex list-unstyled mt-auto">
              <li class="me-auto">
                <img src="https://github.com/twbs.png" alt="Bootstrap" width="32" height="32" class="rounded-circle border border-white">
              </li>
              <li class="d-flex align-items-center me-3">
                <svg class="bi me-2" width="1em" height="1em"><use xlink:href="#geo-fill"/></svg>
                <small>California</small>
              </li>
              <li class="d-flex align-items-center">
                <svg class="bi me-2" width="1em" height="1em"><use xlink:href="#calendar3"/></svg>
                <small>5d</small>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container px-4 py-5" id="custom-cards">
     <h1 class="text-center mt-4 text-white">
       <span class="rounded p-2 fontdesign3">講</span>
       <span class="rounded p-2 fontdesign3">座</span>
       <span class="rounded p-2 fontdesign3">活</span>
       <span class="rounded p-2 fontdesign3">動</span>
      </h1>




    <div class="row row-cols-1 row-cols-lg-3 align-items-stretch g-4 py-5">
      <div class="col">
        <div class="card card-cover h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg" style="background-image: url('unsplash-photo-1.jpg');">
          <div class="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1">
            <h2 class="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold">Short title, long jacket</h2>
            <ul class="d-flex list-unstyled mt-auto">
              <li class="me-auto">
                <img src="https://github.com/twbs.png" alt="Bootstrap" width="32" height="32" class="rounded-circle border border-white">
              </li>
              <li class="d-flex align-items-center me-3">
                <svg class="bi me-2" width="1em" height="1em"><use xlink:href="#geo-fill"/></svg>
                <small>Earth</small>
              </li>
              <li class="d-flex align-items-center">
                <svg class="bi me-2" width="1em" height="1em"><use xlink:href="#calendar3"/></svg>
                <small>3d</small>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div class="col">
        <div class="card card-cover h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg" style="background-image: url('unsplash-photo-2.jpg');">
          <div class="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1">
            <h2 class="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold">Much longer title that wraps to multiple lines</h2>
            <ul class="d-flex list-unstyled mt-auto">
              <li class="me-auto">
                <img src="https://github.com/twbs.png" alt="Bootstrap" width="32" height="32" class="rounded-circle border border-white">
              </li>
              <li class="d-flex align-items-center me-3">
                <svg class="bi me-2" width="1em" height="1em"><use xlink:href="#geo-fill"/></svg>
                <small>Pakistan</small>
              </li>
              <li class="d-flex align-items-center">
                <svg class="bi me-2" width="1em" height="1em"><use xlink:href="#calendar3"/></svg>
                <small>4d</small>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div class="col">
        <div class="card card-cover h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg" style="background-image: url('unsplash-photo-3.jpg');">
          <div class="d-flex flex-column h-100 p-5 pb-3 text-shadow-1">
            <h2 class="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold">Another longer title belongs here</h2>
            <ul class="d-flex list-unstyled mt-auto">
              <li class="me-auto">
                <img src="https://github.com/twbs.png" alt="Bootstrap" width="32" height="32" class="rounded-circle border border-white">
              </li>
              <li class="d-flex align-items-center me-3">
                <svg class="bi me-2" width="1em" height="1em"><use xlink:href="#geo-fill"/></svg>
                <small>California</small>
              </li>
              <li class="d-flex align-items-center">
                <svg class="bi me-2" width="1em" height="1em"><use xlink:href="#calendar3"/></svg>
                <small>5d</small>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</article>



<footer class="footer1">
<div class="container">

  <div class="row center">
    <div class="col-4">
          
      <ul class="nav flex-column text-white">
      
        <li><h6>不想錯過最新課程資訊？</h6></li>
        <li><h2>訂閱</h2></li>
        <li><h2>電子報</h2></li>
      </ul>
    </div>
    <div class="col-4">
      <ul class="nav flex-column">
        <div class="d-flex w-100 gap-2"><form>
          <li>
            <label class="visually-hidden">Name</label>
            <input id="name" type="text" class="form-control emailstyle" placeholder="Name">
          </li>
          <br>
          <li>
            <label class="visually-hidden">Email address</label>
            <input id="email" type="text" class="form-control emailstyle" placeholder="Email address">
          </li>
          <br>
          <li>
            <button class="btn btn-primary emailstyle" type="button">提交</button>
          </li>
        </div>
      </form>
      </ul>

    </div>
  </div>
</div>

</footer>

  <footer class="py-5 bg-success">
  <div class="container">
  
      <div class="row">
        <div class="col-4">
          
          <ul class="nav flex-column">
            <li class="nav-item mb-2 footername1">香港生態旅遊專業培訓中心<li>
              <li class="nav-item mb-2 footername2">Hong Kong Ecotourism & Travels Professional Training Centre</li>
              <li class="nav-item mb-2 address">九龍荔枝角青山道682-684號 潮流工貿中心30樓9-10室</li>
              <li class="nav-item mb-2 address">Tel:(852) 2116 1971    Fax:(852) 2465 9986</li>
              <li class="nav-item mb-2 address">Email:info@ettc.hk</li>
          </ul>
        </div>
        
        <div class="col-1" >        
          <ul class="nav flex-column" >
            <li class="nav-item mb-2"  ><a href="#" class="nav-link p-0 text-white">關於我們</a></li>
            <li class="nav-item mb-2"><a href="index2.html" class="nav-link p-0 text-white">課程總覽</a></li>
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">講座活動</a></li>           
          </ul>
        </div>
        <div class="col-1 ">
         <ul class="nav flex-column">
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">包班/頂目</a></li>
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">活動花絮</a></li>
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">常目問題</a></li>
          
          </ul>
        </div>
        <div class="col-1 ">
          <ul class="nav flex-column">
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">學員專區</a></li>
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">導師專區</a></li>
          </ul>
        </div>
      </div>
    </div>
      

      <div class="border-top justify-content-between">
      <!-- <div class="d-flex justify-content-between py-4 my-4 border-top"> -->
        <p>&copy; 2021 Company, Inc. All rights reserved.</p>
        <!-- <ul class="list-unstyled d-flex">
          <li class="ms-3"><a class="link-dark" href="#"><svg class="bi" width="24" height="24"><use xlink:href="#twitter"/></svg></a></li>
          <li class="ms-3"><a class="link-dark" href="#"><svg class="bi" width="24" height="24"><use xlink:href="#instagram"/></svg></a></li>
          <li class="ms-3"><a class="link-dark" href="#"><svg class="bi" width="24" height="24"><use xlink:href="#facebook"/></svg></a></li>
        </ul> -->
      </div>
    </footer>

  



    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script> -->

      
  </body>
</html>
