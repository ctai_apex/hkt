<?php 
header("Content-Type:text/html; charset=utf-8");
session_start();
if(!empty($_SESSION["name"])){
?>

<!doctype html>
<html lang="en">
<header>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">


  <!-- Bootstrap Core CSS -->
  <link href="../css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="../css/metisMenu.min.css" rel="stylesheet">

<!-- DataTables CSS -->
<link href="../css/dataTables/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="../css/dataTables/dataTables.responsive.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="../css/startmin.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->


 <!-- DataTables CSS -->
 <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.1/css/dataTables.bootstrap4.min.css"/>
 <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"/> -->

    <link href="../css/course.css" rel="stylesheet" type="text/css">



</header>



<body>



<div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-4">
                        <h1 class="page-header"> Records</h1>
                    </div><div class="col-lg-4">
                        <h1 class="page-header"> <a href="inputcourse.php"><button>Add</button></a></h1>
                    </div>
                  </div> 
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="dataTable_wrapper">



<?php
echo "<table class='table table-striped table-bordered table-hover' id='dataTables-example'>";
// echo "<table id='example' class='table table-striped table-bordered' style='width:100%'>";
echo "<thead>
        <tr>
            <th>ID</th>
            <th>名稱</th>
            <th>課程主題</th>
            <th>課程特色</th>
            <th>課程目標</th>
            <th>課程大綱</th>
            <th>課程對象 </th>
            <th>相關資訊</th>
            <th>課程時間表</th>
            <th>導師及顧問</th>
            <th>報名須知</th>
            <th>CEF</th>
             <th>學員</th> 
            
           
        </tr>
        </thead> 
        
        <tbody>";

$servername = "localhost";
$username = "carson"; 
$password = "64329681";
$dbname = "hkt";

try {
            $conn = new PDO("mysql:host=$servername;dbname=$dbname;charset=UTF8", $username, $password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $conn->prepare("SELECT * FROM hkt_course ORDER BY _c_id"); 
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC); 
            foreach($stmt->fetchAll() as $row){
            ?>



            
                <tr>
                      

                <td>
                          <?PHP echo $row["_c_id"] ?>
                           <a href="<?PHP echo './editcourse.php?_c_id='.$row['_c_id'] ?>"><button>Edit</button></a>
                        </td>
                        <td >
                         <?PHP echo $row["_c_name"] ?> 
                        </td>
                        <td >
                        <textarea rows="5" cols="15" readonly><?PHP echo $row["_c_title"] ?></textarea>
                        </td>
                        <td>
                           <textarea rows="5" cols="15" readonly><?PHP echo $row["_c_description"] ?> </textarea>
                        </td>
                       <td >
                       <textarea rows="5" cols="10" readonly> <?PHP echo $row["_c_goal"] ?> </textarea>
                            
                        </td>
                       
                        <td >
                        <textarea rows="5" cols="15" readonly><?PHP echo $row["_c_introduction"] ?> </textarea>
                        </td>
                        <td >
                        <textarea rows="5" cols="10" readonly><?PHP echo $row["_c_towho"] ?> </textarea>
                        </td>
                        <td >
                        <textarea rows="5" cols="15" readonly><?PHP echo $row["_c_info"] ?> </textarea>
                        </td>
                       
                        <td >
                        <textarea rows="5" cols="10" readonly>  <?PHP echo $row["_c_timetable"] ?></textarea>
                        </td>
                        <td >
                        <textarea rows="5" cols="10" readonly>  <?PHP echo $row["_c_teacher"] ?> </textarea>
                        </td>
                        <td >
                        <textarea rows="5" cols="10" readonly><?PHP echo $row["_c_submitinfo"] ?></textarea>
                        </td>
                               
                        <td >
                        <a href="<?PHP echo './editcourse.php?_c_id='.$row['_c_id'] ?>"><button>Edit</button></a>
                         </td>
                        <td >
                        <a href="<?PHP echo './editcourse.php?_c_id='.$row['_c_id'] ?>"><button>Edit</button></a>
                     </td>
                        <!-- <td class="ovflow">
                            <?PHP echo $row["_c_createtime"] ?>
                        </td> -->


                     
                        
                        <!-- <td style="width:10px;border:1px solid black;">
                        <a href="<?PHP echo './editcourse.php?_c_id='.$row['_c_id'] ?>"><button>Edit</button></a>
                        </td> -->
                  
                    </tr>






<!-- jQuery -->
<script src="../js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../js/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="../js/dataTables/jquery.dataTables.min.js"></script>
<script src="../js/dataTables/dataTables.bootstrap.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../js/startmin.js"></script>


<?PHP
                    }
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;
echo "</tbody></table>";
?>

                        
                     </div>   
               </div> 
           </div>
        </div> 
    </div> 
</div>
<!-- 
<script >
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script> -->

<script>
            $(document).ready(function() {
                $('#dataTables-example').DataTable({
                        responsive: true,
                        //  "order": [[ 1, "desc"]]
                });
            });
        </script>

</body>
</html>

<?PHP
}else{
    header("Refresh:3; url=../server_userdata/login.html");
}
?>