<!doctype html>
<html lang="en">
    <header>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
  
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
 

    <style>.multiselect {
  width: 200px;
}

.selectBox {
  position: relative;
}

.selectBox select {
  width: 100%;
  font-weight: bold;
}

.overSelect {
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
}

#checkboxes {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes label {
  display: block;
}

#checkboxes label:hover {
  background-color: #1e90ff;
}
      </style>
    </header>

    <body>

<div class="container">
  <div class="py-5 text-center">
      <h2>Checkout form</h2>
      </div>

 

  <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                         <form role="form" action="input_process.php" method="POST" enctype="multipart/form-data">
                                           
                                         <div class="form-group">
                                         <label>課程名稱 :</label>
                                           <input class="form-control" name="_c_name" value="">
                                        </div>
                                            
                                            <div class="form-group">
                                                <label>課程主題 :</label>
                                                <textarea class="form-control" name="_c_title" value=""></textarea>
                                            </div>

                                            <div class="form-group">
                                                <label>課程特色 : </label>
                                                <textarea class="form-control" name="_c_description" value=""></textarea>
                                            </div>

                                            <div class="form-group">
                                                <label>課程目標 :</label>
                                                <textarea class="form-control" name="_c_goal" value=""></textarea>
                                            </div>

                                            <div class="form-group">
                                                <label>課程大綱 :</label>
                                                <textarea class="form-control" name="_c_introduction" value=""></textarea>
                                            </div>

                                            <div class="form-group">
                                                <label>課程對象 :</label>
                                                <textarea class="form-control" name="_c_towho" value=""></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>相關資訊 :</label>
                                                <textarea class="form-control" name="_c_info" value=""></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>課程時間表 :</label>
                                                <textarea class="form-control" name="_c_timetable" value=""></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>導師及顧問 :</label>
                                              
  <form>
  <div class="multiselect">
    <div class="selectBox" onclick="showCheckboxes()">
      <select>
        <option>Select an option</option>
      </select>
      <div class="overSelect"></div>
    </div>
    <div id="checkboxes">
      <label for="one">
        <input type="checkbox" id="one" />First checkbox</label>
      <label for="two">
        <input type="checkbox" id="two" />Second checkbox</label>
      <label for="three">
        <input type="checkbox" id="three" />Third checkbox</label>
    </div>
  </div>
</form>

                                            </div>  
                                            <div class="form-group">
                                                <label>報名須知 :</label>
                                                <textarea class="form-control" name="_c_submitinfo" value=""></textarea>
                                            </div>

                                            <div class="form-group">
                                                <label>CEF 注意事項 :</label>
                                                <textarea class="form-control" name="_c_cef" value=""></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>學員心聲 :</label>
                                                <textarea class="form-control" name="_c_studentmessage" value=""></textarea>
                                            </div>
                                            <!-- <input type="hidden" name="_c_id" value=""> -->

                                            <button style="background-color:lightgreen" type="submit" class="btn btn-default">Submit Button</button>
                                            <button type="reset" class="btn btn-default bg-danger">Reset Button</button>
                                        </form>
                                    </div>
                                    <!-- /.col-lg-6 (nested) -->
                                    
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

</div>



<script src="../js/ckeditor/ckeditor.js"></script>
        <script>
            CKEDITOR.replace('_c_title');
            CKEDITOR.replace('_c_description');
            CKEDITOR.replace('_c_goal');
            CKEDITOR.replace('_c_introduction');
            CKEDITOR.replace('_c_towho');
            CKEDITOR.replace('_c_info');
            CKEDITOR.replace('_c_timetable');
            CKEDITOR.replace('_c_submitinfo');
            CKEDITOR.replace('_c_cef');
            CKEDITOR.replace('_c_studentmessage');

        </script>

<script>
  var expanded = false;

function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}
</script>

</body>
</html>