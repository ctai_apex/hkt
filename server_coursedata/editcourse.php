<?php 
header("Content-Type:text/html; charset=utf-8");
session_start();
if(!empty($_SESSION["name"])){
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>   </title>

        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div id="wrapper">

           
           

            <div id="page-wrapper">
                <div class="row">
                   
                                     
                                            <?PHP
                                            $servername = "localhost";
                                            $username = "carson";
                                            $password = "64329681";
                                            $dbname = "hkt";
                                            
                                            try {
                                                $conn = new PDO("mysql:host=$servername;dbname=$dbname;charset=UTF8", $username, $password);
                                                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                                $stmt = $conn->prepare("SELECT * FROM hkt_course WHERE _c_id=:_c_id");
                                                $stmt->bindParam(':_c_id', $_c_id);
                                                $_c_id = $_GET['_c_id'];
                                                $stmt->execute();
                            
                                                $result = $stmt->fetchAll();
                                            }
                                            catch(PDOException $e) {
                                                echo "Error: " . $e->getMessage();
                                            }
                                            $conn = null;
                
                                            ?>
                                             <div class="col-lg-12">
                        <h1 class="page-header">Edit Record</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                         <form role="form" action="update_process.php" method="POST" enctype="multipart/form-data">
                                           
                                         <div class="form-group">
                                         <label>課程名稱 :</label>
                                           <input class="form-control" name="_c_name" value="<?PHP echo $result[0]["_c_name"] ?>"><?PHP echo $result[0]["_c_name"]?> 
                                        </div>
                                            
                                            <div class="form-group">
                                                <label>課程主題 :</label>
                                                <textarea class="form-control" name="_c_title" value="<?PHP echo $result[0]["_c_title"] ?>"><?PHP echo $result[0]["_c_title"] ?></textarea>
                                            </div>

                                            <div class="form-group">
                                                <label>課程特色 : </label>
                                                <textarea class="form-control" name="_c_description" value="<?PHP echo $result[0]["_c_description"] ?>"><?PHP echo $result[0]["_c_description"] ?></textarea>
                                            </div>

                                            <div class="form-group">
                                                <label>課程目標 :</label>
                                                <textarea class="form-control" name="_c_goal" value="<?PHP echo $result[0]["_c_goal"] ?>"><?PHP echo $result[0]["_c_goal"] ?></textarea>
                                            </div>

                                            <div class="form-group">
                                                <label>課程大綱 :</label>
                                                <textarea class="form-control" name="_c_introduction" value="<?PHP echo $result[0]["_c_introduction"] ?>"><?PHP echo $result[0]["_c_introduction"] ?></textarea>
                                            </div>

                                            <div class="form-group">
                                                <label>課程對象 :</label>
                                                <textarea class="form-control" name="_c_towho" value="<?PHP echo $result[0]["_c_towho"] ?>"><?PHP echo $result[0]["_c_towho"] ?></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>相關資訊 :</label>
                                                <textarea class="form-control" name="_c_info" value="<?PHP echo $result[0]["_c_info"] ?>"><?PHP echo $result[0]["_c_info"] ?></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>課程時間表 :</label>
                                                <textarea class="form-control" name="_c_timetable" value="<?PHP echo $result[0]["_c_timetable"] ?>"><?PHP echo $result[0]["_c_timetable"] ?></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>導師及顧問 :</label>
                                                
                                            </div>
                                            <div class="form-group">
                                                <label>報名須知 :</label>
                                                <textarea class="form-control" name="_c_submitinfo" value="<?PHP echo $result[0]["_c_submitinfo"] ?>"><?PHP echo $result[0]["_c_submitinfo"] ?></textarea>
                                            </div>

                                            <div class="form-group">
                                                <label>CEF 注意事項 :</label>
                                                <textarea class="form-control" name="_c_cef" value="<?PHP echo $result[0]["_c_cef"] ?>"><?PHP echo $result[0]["_c_cef"] ?></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>學員心聲 :</label>
                                                <textarea class="form-control" name="_c_studentmessage" value="<?PHP echo $result[0]["_c_studentmessage"] ?>"><?PHP echo $result[0]["_c_studentmessage"] ?></textarea>
                                            </div>
                                            <input type="hidden" name="_c_id" value="<?PHP echo $_GET['_c_id'] ?>">

                                            <button type="submit" class="btn btn-default">Submit Button</button>
                                            <button type="reset" class="btn btn-default">Reset Button</button>
                                        </form>
                                    </div>
                                    <!-- /.col-lg-6 (nested) -->
                                    
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="../js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../js/metisMenu.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="../js/startmin.js"></script>


        <script src="../js/ckeditor/ckeditor.js"></script>
        <script>
            CKEDITOR.replace('_c_title');
            CKEDITOR.replace('_c_description');
            CKEDITOR.replace('_c_goal');
            CKEDITOR.replace('_c_introduction');
            CKEDITOR.replace('_c_towho');
            CKEDITOR.replace('_c_info');
            CKEDITOR.replace('_c_timetable');
            CKEDITOR.replace('_c_submitinfo');
            CKEDITOR.replace('_c_cef');
            CKEDITOR.replace('_c_studentmessage');

        </script>

    </body>
</html>

<?PHP
    
}else{
    
   
//     header("Refresh:3; url=ologin.html");
    }

?>