<?php
require_once('conf.php');
require_once('lang.php');

$privacy = 'public';

//====Load all calendars
$allCals = new C_Calendar('LOAD_PUBLIC_CALENDARS');

//====Load calendar properties
$calendarProperties = $allCals->calendarProperties;

//====Load calendars
$allCalendars = $allCals->allCalendars;

//====Set language/locale setting
define('LANG', $calendarProperties['language']);

//==== Get calendar Id
//$calendarId = isset($_GET['c'])?$_GET['c']:0;

//====Initiate Event Calendar Class
$pec = new C_PhpEventCal($calendarProperties);

//==== Setting Properties
$pec->header();

$pec->buttonText(array(
    'prev'      =>$lang[LANG]['calendar_view_btn_prev'],
    'next'      =>$lang[LANG]['calendar_view_btn_next'],
    'agendaDay' =>$lang[LANG]['calendar_view_btn_agenda_day'],
    'basicDay'  =>$lang[LANG]['calendar_view_btn_basic_day'],
    'month'     =>$lang[LANG]['calendar_view_btn_month'],
    'agendaWeek'=>$lang[LANG]['calendar_view_btn_agenda_week'],
    'list'      =>$lang[LANG]['calendar_view_btn_list'],
    'resourceDay'=>$lang[LANG]['calendar_view_btn_resource_day'],
    'pec'       =>$lang[LANG]['calendar_view_btn_pec'],)
);

//===Each Event as a form of Array
$events = array(
//    array('id'=>178,'title'=>'My Event 1','start'=>'2014-02-10'),
//    array('id'=>178,'title'=>'My Event 2','start'=>'2014-02-17',),
//    array('id'=>178,'title'=>'My Event 3','start'=>'2014-02-24')
);

$pec->events($events);

//==============================================
//TODO:Event Source is not working at the moment
/*
$pec->eventSources(
    array('events'=>$moreEvents,'color'=>'red','textColor'=>'green','backgroundColor'=>'gray')
);
*/
//====================================================
//TODO: Google Event Feed is not working at the moment
//$pec->events('http://www.google.com/calendar/feeds/developer-calendar@google.com/public/full?alt=json-in-script','json');

$pec->editable(true);

$pec->dragOpacity(.2);
?>

<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PHP Event Calendar - Public (Demo)</title>
    <?php echo $pec->display('head');?>
    <style>
        .container {
            width: auto;
            font-family:'Helvetica neue', Helvetica, Arial, sans-serif;

        }
        #add-calendar {
            cursor: pointer;
        }
        .list-group a {
            padding: 4px;
            text-align: left;
            padding-left: 10px;
            padding-right: 2px;
        }
        .list-group a:hover {
            opacity: 0.75;
        }
        .fc-header-right .fc-header-space {
            display: none;
        }
        .unselect-calendar {
            float: right;
            font-size: 8px;
            margin-top: 13px;
            display: inline-block;
            z-index: 10000;
        }
        .unselect-calendar:hover {
            text-shadow: 0 2px 5px black;
            color: maroon;
        }

        a#cal-settings-link{
            display: none;
        }

        #calendar > table > tbody > tr > td.fc-header-right > span.fc-button.fc-button-resourceDay.fc-state-default.fc-corner-left.fc-corner-right{
            display:none;
        }
    </style>
</head>

<body>
<?php require_once(SERVER_HTML_INCLUDE_DIR.'top-navigation.html.php'); ?>
<div class="container">
    <div class="starter-template">
        <p class="lead">
        <div class="row">
            <div class="col-md-2 nav">
                <!-- Create New Event Button -->
                <div style="float: left; padding-left: 10px;">
                    <button type="button" class="btn btn-success disabled" style="display:none">Create New Event</button>
                </div>
                <div style="clear: both; height: 17px; display: none"></div>
                <!-- Date Picker -->
                <div id="date-picker" style="border: 1px solid #d9d9d9; margin-left: 3px; margin-top: 0; padding-top: 0; border-radius: 2px"></div>


                <!-- My Calendar -->
                <div id="my-calendars" class="panel panel-default" style="margin-top: 10px; margin-left: 3px;">
                    <div class="panel-heading">
                        <h3 class="panel-title" style="width: 100%">My Calendars <span id="add-calendar" class="glyphicon glyphicon-plus" style="float: right; margin-left: 8px;display:none"></span>&nbsp;<span id="manage-calendar" class="glyphicon glyphicon-cog" style="margin-top:1px; float: right; cursor: pointer;display:none"></span></h3>
                    </div>
                    <div class="list-group" id="list-group-public">

                        <?php if($allCalendars != NULL) foreach($allCalendars as $k => $v){ ?>
                            <?php
                            //var_dump($calid);
                            if($v['id']){
                                $active = '<span class="glyphicon glyphicon-remove unselect-calendar"></span><span style="float: right" class="glyphicon glyphicon-ok"></span>';
                                $activeClass = 'selected';
                            }
                            else {
                                $active = '';
                                $activeClass = '';
                            }
                            ?>
                            <a href="javascript:void(0);" class="list-group-item list-group-item-public ladda-button <?php echo $activeClass?>" data-style="expand-right" style="background-color: <?php echo $v['color']?>; color:white;" id="<?php echo $v['id']?>"><span class="ladda-label"><?php echo $v['name']?></span> <?php echo $active?></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-md-10" style="overflow:hidden;float:inherit;width:inherit">
                <?php
                $pec->display_container();
                ?>
            </div>
        </div>
        </p>
    </div>

</div><!-- /.container -->

<?php
//=====display
$pec->display('body','public');
?>

</body>
</html>