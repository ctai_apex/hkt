<?php
require_once("../conf.php");
?>
<!DOCTYPE html>
<html>
<head>
</head>
<body>
    <style>

        body {
            margin-top: 40px;
            text-align: center;
            font-size: 14px;
            font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
        }
/*
        #calendar {
            width: 900px;
            margin: 0 auto;
        }
*/
    </style>

<?php
$pec = new C_PhpEventCal();

//==== Setting Properties
$pec->header();
//$pec->firstDay(2);
$pec->weekends();
$pec->weekMode('liquid');
$pec->weekNumbers(true);
$pec->height(580);
$pec->contentHeight(400);
$pec->slotMinutes(50);
$pec->defaultView('month'); //month,basicWeek,agendaWeek,basicDay,agendaDay
$pec->buttonText(array('prev'=>'&lt;','next'=>'&gt;'));
//$pec->allDaySlot(true);
//$pec->fcFunction('viewRender',array());
//$pec->handleWindowResize(true);

//=====display
$pec->display();

?>

</body>
</html>
