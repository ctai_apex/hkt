<?php
    require_once('conf.php');
    unset($_SESSION['userData']);
    @session_destroy();
    setcookie("currentView", "");
    header('Location: index.php');