<?php
/**
 * File: cls_core.php: Some core functionalities related to front end as well as back end
 *
 * Description: It extends event properties and has some important core functionalities.
 *
 * @package eventcalendar
 * @author Richard Z.C. <info@phpeventcalendar.com>
 *
 * @version beta-1.0.2
 * @copyright 2014, phpeventcalendar.com
 * @filesource
 */

/**
 * Class C_Core : Front End methods for loading javascript libraries as well as loading CSS libraries also it loads DB object
 *
 * Description: It extends Event properties. The main task of this class is to load javascript libraries and css files for
 * front end purposes. Beside it loads DB object.
 *
 * @author: Richard Z.C. <info@phpeventcalendar.com>
 * @package eventcalendar
 * @version beta-1.0.2
 *
 */

class C_Core extends C_Properties
{
    /**
     * @var object DB Object
     */
    public $db;

    /**
     * Constructor Function
     */
    public function __construct()
    {
    }

    /**
     * Loads All CSS files based on user requests for front end purposes
     *
     * @param bool $bootstrap, if true then loads the CSS for bootstrap
     * @param bool $fullCalendar, if true then loads the CSS for fullCalendar
     * @param bool $datetimePicker, if true then loads the CSS for datetimePicker
     * @param bool $colorPicker, if true then loads colorPicker
     * @param bool $jqueryUI, if true then loads jqueryUI
     *
     * @author: Richard Z.C. <info@phpeventcalendar.com>
     *
     */
    public static function display_script_include_once_head($bootstrap = false, $fullCalendar = false, $datetimePicker = false, $colorPicker = false, $jqueryUI = false)
    {
        if ($fullCalendar) {
            echo '<link rel="stylesheet" href="' . PEC_PATH . '/css' . FULL_CALENDAR_VERSION  . '/fullcalendar.css" />' . "\n";
            echo '<link rel="stylesheet" href="' . PEC_PATH . '/css' . FULL_CALENDAR_VERSION  . '/fullcalendar.print.css" media="print"/>' . "\n";
            echo '<link rel="stylesheet" href="' . PEC_PATH . '/css' . FULL_CALENDAR_VERSION  . '/fullcalendar.custom.css" />' . "\n";
            echo '<link rel="stylesheet" href="' . PEC_PATH . '/css/pec-common.css" />' . "\n";
        }
        if ($bootstrap) {
            echo '<link rel="stylesheet" href="' . PEC_PATH . '/plugins/bootstrap/css/bootstrap.min.css" />' . "\n";
            echo '<link rel="stylesheet" href="' . PEC_PATH . '/plugins/bootstrap/css/bootstrap-theme.min.css" />' . "\n";
        }

        if ($datetimePicker) {
            echo '<link rel="stylesheet" href="' . PEC_PATH . '/plugins/bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.min.css" />' . "\n";
        }
        if ($colorPicker) {
            echo '<link rel="stylesheet" href="' . PEC_PATH . '/plugins/bootstrap-colorpicker-master/css/bootstrap-colorpicker.min.css" />' . "\n";
        }
        if ($jqueryUI) {
            echo '<link rel="stylesheet" href="' . PEC_PATH . '/css' . FULL_CALENDAR_VERSION  . '/start/jquery-ui.min.css" />';
        }

        echo '<link rel="stylesheet" href="' . PEC_PATH . '/plugins/ladda-bootstrap-master/dist/ladda-themeless.min.css" />' . "\n";
        echo '<link rel="stylesheet" href="' . PEC_PATH . '/plugins/bootstrap-silviomoreto-select/css/bootstrap-select.min.css" />' . "\n";
        echo '<link rel="stylesheet" href="' . PEC_PATH . '/css/file-uploader/jquery.fileupload.css" />' . "\n";
        echo '<link rel="shortcut icon" href="'. PEC_PATH  .'/images/pec-logo-icon.png"/>'. "\n";

        //echo '<link rel="stylesheet" href="' . PEC_PATH . '/plugins/bootstrap-select2/select2.css" />' . "\n";
    }

    /**
     * Loads Javascript/jQuery libraries for Front end purposes based on user request. Other than requested libraries
     * it loads few Javascript/jQuery plugins by default
     *
     * @param bool $bootstrap, if true then loads bootstrap jquery library
     * @param bool $fullCalendar, if true then loads fullCalendar jquery library
     * @param bool $datetimePicker, if true then loads datetimePicker jquery library
     * @param bool $colorPicker, if true then loads colorPicker
     * @param bool $jqueryUI, if true then loads jqueryUI jquery library
     *
     * @author: Richard Z.C. <info@phpeventcalendar.com>
     */
    public static function display_script_include_once_foot($bootstrap = false, $fullCalendar = false, $datetimePicker = false, $colorPicker = false, $jqueryUI = false)
    {
        echo '<script src="' . PEC_PATH . '/js' . FULL_CALENDAR_VERSION  . '/jquery.min.js" type="text/javascript"></script>' . "\n";
        //if(FULL_CALENDAR_VERSION == '/fullcalendar-2.0.0') {
            echo '<script src="' . PEC_PATH . '/js' . FULL_CALENDAR_VERSION  . '/moment.min.js" type="text/javascript"></script>' . "\n";
        //}
        if ($jqueryUI) {
            echo '<script src="' . PEC_PATH . '/js' . FULL_CALENDAR_VERSION  . '/jquery-ui.custom.min.js" type="text/javascript"></script>' . "\n";
        }
        if ($bootstrap) {
            echo '<script src="' . PEC_PATH . '/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>' . "\n";
        }
        if ($datetimePicker) {
            echo '<script src="' . PEC_PATH . '/plugins/bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>' . "\n";
            if (defined('LANG') && LANG != 'en') echo '<script src="' . PEC_PATH . '/plugins/bootstrap-datetimepicker-master/js/locales/bootstrap-datetimepicker.'. LANG .'.js" type="text/javascript"></script>' . "\n";
        }
        if ($colorPicker) {
            echo '<script src="' . PEC_PATH . '/plugins/bootstrap-colorpicker-master/js/bootstrap-colorpicker.min.js" type="text/javascript"></script>' . "\n";
        }
        if ($fullCalendar) {
            echo '<script src="' . PEC_PATH . '/js' . FULL_CALENDAR_VERSION  . '/fullcalendar.js" type="text/javascript"></script>' . "\n";
            if (defined('LANG') && LANG != 'en') echo '<script src="' . PEC_PATH . '/js' . FULL_CALENDAR_VERSION  . '/lang/'. LANG .'.js" type="text/javascript"></script>' . "\n";
        }
        echo '<script src="' . PEC_PATH . '/plugins/ifightcrime-bootstrap-growl/jquery.bootstrap-growl.min.js" type="text/javascript"></script>' . "\n";
        echo '<script src="' . PEC_PATH . '/plugins/ladda-bootstrap-master/dist/spin.min.js" type="text/javascript"></script>' . "\n";
        echo '<script src="' . PEC_PATH . '/plugins/ladda-bootstrap-master/dist/ladda.min.js" type="text/javascript"></script>' . "\n";
        echo '<script src="' . PEC_PATH . '/plugins/bootstrap-silviomoreto-select/js/bootstrap-select.min.js" type="text/javascript"></script>' . "\n";
        
        if (defined('LANG') && LANG != 'en') echo '<script src="' . PEC_PATH . '/plugins/bootstrap-silviomoreto-select/js/i18n/defaults-'. LANG .'_'. strtoupper(LANG) .'.min.js" type="text/javascript"></script>' . "\n";

        //-------------------------------File Uploader start ---------------------------------------------
        echo '<script src="' . PEC_PATH . '/js/file-uploader/vendor/load-image.all.min.js" type="text/javascript"></script>' . "\n";
        echo '<script src="' . PEC_PATH . '/js/file-uploader/vendor/jquery.ui.widget.js" type="text/javascript"></script>' . "\n";
        echo '<script src="' . PEC_PATH . '/js/file-uploader/jquery.fileupload.js" type="text/javascript"></script>' . "\n";
        echo '<script src="' . PEC_PATH . '/js/file-uploader/jquery.fileupload-process.js" type="text/javascript"></script>' . "\n";
        echo '<script src="' . PEC_PATH . '/js/file-uploader/jquery.fileupload-image.js" type="text/javascript"></script>' . "\n";
        echo '<script src="' . PEC_PATH . '/js/file-uploader/jquery.fileupload-validate.js" type="text/javascript"></script>' . "\n";
        //-------------------------------File Uploader end ---------------------------------------------
        
        if ($fullCalendar) {        
            echo '<script src="' . PEC_PATH . '/js' . FULL_CALENDAR_VERSION  . '/gcal.js" type="text/javascript"></script>' . "\n";   
        }     
    }

    /**
     * Loads custom JS for Front End purposes
     * @param $jsName
     * @author: Richard Z.C. <info@phpeventcalendar.com>
     */
    public static function display_custom_js($jsName)
    {
        global $lang;
        
        require_once $_SERVER['DOCUMENT_ROOT'] . PEC_PATH . "/js/custom/$jsName.js.php";
    }

    /**
     * Loads DB Object
     *
     * Note: This object wont be inherited for any static method
     * @author: Richard Z.C. <info@phpeventcalendar.com>
     */
    public function load_db()
    {
        $this->db = new C_Database(PEC_DB_HOST, PEC_DB_USER, PEC_DB_PASS, PEC_DB_NAME, PEC_DB_TYPE, PEC_DB_CHARSET);
    }

    public static function sendEmail($recipients, $emailSubject, $emailBody, $CC='', $BCC='', $files=''){
        define('ENABLE_OUTGOING_EMAILS',true);
        if(ENABLE_OUTGOING_EMAILS){

            $baseurl = str_replace('\\','/',BASE_DIR);
            require_once ($baseurl.'server/PHPMailer/PHPMailerAutoload.php');

            //-------------------------------------------------------------------------------------
            //--- create phpmail object
            $mail = new PHPMailer;
            $mail->isSMTP();
            $mail->Host = '';
            $mail->SMTPAuth = true;
            $mail->Username = '';
            $mail->Password = '';
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->From = '';
            $mail->FromName = '';
            // $mail->addAddress('myemail@example.com', 'My name');
            $mail->isHTML(true);
            //-------------------------------------------------------------------------------------
            //--- prepare email body and subject
            $mail->Body = $emailBody;
            $mail->Subject = $emailSubject;
            //-------------------------------------------------------------------------------------
            //--- Add Email Recipients
            if(!empty($recipients)){
                if(strstr($recipients, ",")){
                    $recipients = explode(",", $recipients);
                }
                elseif(strstr($recipients, ";")){
                    $recipients = explode(";", $recipients);
                }
                else{
                    $recipients = array($recipients);
                }

                foreach($recipients as $email){
                    $mail->AddAddress(@trim($email));
                }
            }
            //-------------------------------------------------------------------------------------
            //--- Add CC Recipients
            if(!empty($CC)){
                if(strstr($CC, ",")){
                    $CC = explode(",", $CC);
                }
                elseif(strstr($CC, ";")){
                    $CC = explode(";", $CC);
                }
                else{
                    $CC = array($CC);
                }

                foreach($CC as $email){
                    $mail->AddCC(@trim($email));
                }
            }
            //-------------------------------------------------------------------------------------
            //--- Add BCC Recipients
            if(!empty($BCC)){
                if(strstr($BCC, ",")){
                    $BCC = explode(",", $BCC);
                }
                elseif(strstr($BCC, ";")){
                    $BCC = explode(";", $BCC);
                }
                else{
                    $BCC = array($BCC);
                }

                foreach($BCC as $email){
                    $mail->AddBCC(@trim($email));
                }
            }
            //-------------------------------------------------------------------------------------
            //--- Process Attachments if any
            if(!empty($files)){
                //----- Find key of files object
                $tmp = array_keys($files);
                $key = $tmp[0];
                $totalFiles = count($files[$key]['name']);

                if($totalFiles > 0){
                    for($i=0; $i<$totalFiles; $i++){
                        $mail->AddAttachment($files[$key]['tmp_name'][$i], $files[$key]['name'][$i]);
                    }
                }
            }
            //-------------------------------------------------------------------------------------
            //--- send email(s)
            if($mail->Send()) return 'sent';
            else return $mail->ErrorInfo;
        }
        else{
            return 'sent';
        }
    }



}