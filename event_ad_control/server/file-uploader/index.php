<?php
/*
 * jQuery File Upload Plugin PHP Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

error_reporting(E_ALL | E_STRICT);
require('UploadHandler.php');
//$upload_handler = new UploadHandler();
//echo $upload_handler->get_server_var('/server/file-uploader/');
//echo $custom_dir = $_SERVER['DOCUMENT_ROOT'];

$https = !empty($_SERVER['HTTPS']) && strcasecmp($_SERVER['HTTPS'], 'on') === 0 ||
    !empty($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
    strcasecmp($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') === 0;


$custom_dir = $_REQUEST['custom_dir'];
$localhost = ($https ? 'https://' : 'http://').$_SERVER['SERVER_NAME'];
$host = $_SERVER['DOCUMENT_ROOT'];
//echo $upload_dir = str_replace($localhost,$host,$custom_dir);
$upload_dir = $host.$custom_dir;
$upload_handler = new UploadHandler(array('upload_dir' => $upload_dir));
//echo $_SERVER['HTTP_REFERER'];