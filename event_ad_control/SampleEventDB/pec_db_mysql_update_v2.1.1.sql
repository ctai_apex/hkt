-- language default to 'en' matching the fullcalendar lang file
ALTER TABLE `pec_settings` CHANGE `language` `language` VARCHAR(64)  CHARACTER SET utf8  COLLATE utf8_general_ci  NULL  DEFAULT 'en';

-- settings for 24 or 12 hour format
ALTER TABLE `pec_settings` ADD `hour_format` VARCHAR(20)  NULL  DEFAULT 'hh:mm A'  COMMENT '24 (HH:mm) or 12 (hh:mm A) hour format based on MomentJs'  AFTER `google_api_key`;